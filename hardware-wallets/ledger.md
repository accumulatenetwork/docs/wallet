# Ledger

The Accumulate Core Wallet supports integration with the Ledger Nano. Generally the only difference between using the Ledger vs using local keys is that you must manually confirm all actions executed with the Ledger. There are a few examples below.

## Generate a key

<figure><img src="../.gitbook/assets/gui-ledger-key.gif" alt=""></figure>

## Send tokens

<figure><img src="../.gitbook/assets/gui-ledger-sendTokens.gif" alt=""></figure>
