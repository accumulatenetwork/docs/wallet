# Managing keys

## Generate a key

### GUI

<figure><img src="../.gitbook/assets/gui-newKey (1) (1).gif" alt=""><figcaption><p>Generate an Accumulate ED25519 key</p></figcaption></figure>

### CLI

<figure><img src="../.gitbook/assets/cli-newKey.png" alt=""><figcaption><p>Generate an Accumulate ED25519 key</p></figcaption></figure>

## Import a key

{% hint style="info" %}
This procedure imports a private key as an external key. To import keys from a mnemonic, [#initializing-the-wallet](setup-your-wallet.md#initializing-the-wallet "mention") or [#create-a-new-vault](managing-vaults.md#create-a-new-vault "mention")
{% endhint %}

### GUI

{% hint style="warning" %}
Key import in the GUI only supports AS1 and Fs formatted keys
{% endhint %}

<figure><img src="../.gitbook/assets/gui-importKey.gif" alt=""><figcaption><p>Import an Accumulate ED25519 key</p></figcaption></figure>

### CLI

{% hint style="warning" %}
Key import in the CLI only supports raw hex keys
{% endhint %}

<figure><img src="../.gitbook/assets/cli-importKey.png" alt=""><figcaption><p>Import an Ethereum key</p></figcaption></figure>
