# Setup your wallet

The default (and simplest) way to use the Accumulate Core Wallet is in
single-vault mode. In this mode, the wallet has a single encryption
key/passphrase and mnemonic, and all keys are stored in the same database. In
this mode, the wallet behaves like a typical crypto wallet; this is sufficient
for most users.

The wallet also supports multi-vault mode. In this mode the wallet maintains
individual, named vaults along with an index database that tracks the vaults.
Each vault has its own encryption key and mnemonic. The index database is also
encrypted. This can be used to keep different sets of keys separate.

## Initializing the wallet

1. Run `accumulate wallet init` or open the GUI.
   - The first time you open the GUI you will be presented with this screen.
2. Choose single- or multi-vault mode.
3. Choose whether to encrypt your wallet. **Encryption is highly recommended.**
4. If you are encrypting your wallet, enter a passphrase.
5. If you are creating a multi-vault wallet, continue with
   [managing-vaults.md](managing-vaults.md "mention").
6. Otherwise, choose to create a new mnemonic or import an existing one.
   **Skipping this step is not recommended**, as most functions of the wallet
   require a mnemonic.
7. Use your new wallet!

### GUI

<figure><img src="../.gitbook/assets/gui-create-wallet.png" alt=""><figcaption></figcaption></figure>

### CLI

<figure><img src="../.gitbook/assets/cli-create-mode.png" alt=""><figcaption><p>Choose vault mode</p></figcaption></figure>

<figure><img src="../.gitbook/assets/cli-create-encrypt.png" alt=""><figcaption><p>Choose whether to encrypt</p></figcaption></figure>

<figure><img src="../.gitbook/assets/cli-create-mnemonic.png" alt=""><figcaption><p>Generate or import a mnemonic</p></figcaption></figure>
