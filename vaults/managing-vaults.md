# Managing vaults

{% hint style="info" %}
With the exception of migrating to multi-vault mode, the operations here are only applicable to a multi-vault wallet.
{% endhint %}


## Migrating from v0.4 and earlier

The wallet will automatically convert the default ≤v0.4 database to a
single-vault wallet. If you were using multiple, separate databases (with the
deprecated `--database` flag), the recommended migration path is to adopt them
into a multi-vault wallet. If you have a database at the default path
(`~/.accumulate/wallet.db` or `~/.accumulate/wallet_encrypted.db`), migrate that
to multi-vault mode after it is automatically converted into a single-vault
wallet. Once that is done, adopt your other databases into the wallet.

It *is* possible to maintain multiple, separate wallets but this is **highly
discouraged** for the majority of users. The only scenario for which this is
recommended is maintaining a separate wallet for use with testnets.



## Migrate to multi-vault mode

The following steps will convert a single-vault wallet into a multi-vault wallet
with a single vault.

1. Run `accumulate vault migrate`.
2. Confirm you wish to convert your wallet to multi-vault mode.
3. Choose what your current vault will be named.
4. Choose whether to encrypt the index database. **Encryption is highly recommended.**
   * This will encrypt the vault list, making it harder to determine which files should be attacked in case a malicious actor gains access to your files.
   * Future versions of the wallet may support indexing of keys, accounts, etc. Encrypting these would prevent information leakage.
5. If you are encrypting your wallet, enter a passphrase.
6. Done!

### GUI

{% hint style="warning" %}
This feature is not yet supported in the GUI
{% endhint %}

### CLI

<figure><img src="../.gitbook/assets/cli-migrate-confirm.png" alt=""><figcaption><p>Confirm</p></figcaption></figure>

<figure><img src="../.gitbook/assets/cli-migrate-name.png" alt=""><figcaption><p>Choose a name</p></figcaption></figure>

<figure><img src="../.gitbook/assets/cli-migrate-encrypt.png" alt=""><figcaption><p>Choose whether to encrypt</p></figcaption></figure>



## Create a new vault

1. Run `accumulate vault create [name]`
2. Choose whether to encrypt the vault. **Encryption is highly recommended.**
3. If you are encrypting your wallet, enter a passphrase.
4. Choose to create a new mnemonic or import an existing one. **Skipping this
   step is not recommended**, as most functions of the wallet require a
   mnemonic.
5. Done!

### GUI

{% hint style="warning" %}
This feature is not yet supported in the GUI
{% endhint %}

### CLI

<figure><img src="../.gitbook/assets/cli-newVault-encrypt.png" alt=""><figcaption><p>Choose whether to encrypt</p></figcaption></figure>

<figure><img src="../.gitbook/assets/cli-newVault-mnemonic.png" alt=""><figcaption><p>Generate or import a mnemonic</p></figcaption></figure>



## Adopt an existing vault

The following steps will adopt an existing vault into your wallet. The target
can be a single-vault wallet or a ≤v0.4 database.

1. Run `accumulate vault adopt [target]`.
2. Confirm you wish to adopt the target into your wallet.
3. Choose what your current vault will be named.
4. Done!

### GUI

{% hint style="warning" %}
This feature is not yet supported in the GUI
{% endhint %}

### CLI

<figure><img src="../.gitbook/assets/cli-adopt-confirm.png" alt=""><figcaption><p>Confirm</p></figcaption></figure>

<figure><img src="../.gitbook/assets/cli-adopt-name.png" alt=""><figcaption><p>Choose a name</p></figcaption></figure>
