# Services

{% hint style="warning" %}
The daemon services are not considered stable; existing methods may change in 0.x minor versions. These changes will be listed in the release notes.
{% endhint %}

## General

Defines methods that do not require access to a vault.

### `Decode`

Decodes an object from binary.

* **Inputs**
  * dataBinary `hex`
* **Outputs**
  * dataJson `any`

### `Encode`

Encodes an object to binary.

* **Inputs**
  * dataJson `any`
* **Outputs**
  * dataBinary `hex`
  * transactionHash `hex`

### `GenerateMnemonic`

Generates a new, random mnemonic.

* **Inputs**
  * entropy `uint64`
* **Outputs**
  * mnemonic `[]string`

### `Version`

Returns the version of the wallet daemon.

* **Inputs**
  * None
* **Outputs**
  * fieldsSet `[]bool` (optional)
  * walletVersion `string`
  * walletCommit `string`
  * databaseVersion `string`
  * extraData `hex` (optional)

## Vault

Manages wallets and vaults.

### `AdoptVault`

Adopts an existing vault into the wallet.

* **Inputs**
  * wallet `string`
  * newVaultName `string`
  * oldVaultPath `string`
* **Outputs**
  * None

### `ConvertWallet`

Converts a single-vault wallet to a multi-vault wallet.

* **Inputs**
  * wallet `string`
  * vault `string`
  * passphrase `string` (optional)
* **Outputs**
  * None

### `CreateVault`

Creates a new vault.

* **Inputs**
  * wallet `string`
  * vault `string`
  * passphrase `string` (optional)
* **Outputs**
  * None

### `CreateWallet`

Initializes the wallet.

* **Inputs**
  * path `string`
  * multiVault `bool` (optional)
  * passphrase `string` (optional)
* **Outputs**
  * None

### `EncryptVault`

Encrypts a previously unencrypted vault.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * passphrase `string`
* **Outputs**
  * None

### `ExportVault`

Returns all the wallet data.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
* **Outputs**
  * vault `*api.Vault`

### `ImportMnemonic`

Imports an existing mnemonic.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * mnemonic `[]string`
* **Outputs**
  * None

### `ImportVault`

Imports an exported vault.

* **Inputs**
  * wallet `string`
  * newVaultName `string` (optional)
  * vault `*api.Vault`
* **Outputs**
  * None

### `ListVaults`

Lists vaults in the wallet.

* **Inputs**
  * wallet `string`
* **Outputs**
  * vaults `[]string`

### `LockVault`

Locks the wallet.

* **Inputs**
  * wallet `string` (optional)
  * vault `string` (optional)
  * close `bool` (optional)
* **Outputs**
  * None

### `OpenVault`

Opens a vault without unlocking it.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
* **Outputs**
  * None

### `Set1PasswordRef`

Updates the 1Password reference for a vault.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * value `string`
* **Outputs**
  * None

### `Status`

Checks the status of the wallets.

* **Inputs**
  * wallet `string`
* **Outputs**
  * wallet `*api.WalletStatus`
  * vaults `[]*api.VaultStatus`

### `UnlockVault`

Unlocks wallet for a certain time.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * use1Password `bool` (optional)
  * passphrase `string` (optional)
  * timeout `time.Duration`
* **Outputs**
  * success `bool`
  * unlockedUntil `time.Time`

## Transaction

Creates and updates stored transactions.

### `AddMemoToTransaction`

Adds a memo to the saved transaction.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * name `string`
  * memo `string`
* **Outputs**
  * name `string`
  * transaction `*protocol.Transaction`

### `AddSendTokensOutput`

Adds an output to the send token transaction.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * name `string`
  * recipient `*protocol.TokenRecipient`
* **Outputs**
  * name `string`
  * transaction `*protocol.Transaction`

### `CreateEnvelope`

Creates an envelope by name.

* **Inputs**
  * name `string`
* **Outputs**
  * name `string`
  * envelope `messaging.Envelope`

### `CreateTransaction`

Creates a transaction by name.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * name `string`
  * principal `*url.URL`
  * path `[]string` (optional)
  * memo `string` (optional)
  * metadata `hex` (optional)
  * getOrCreate `bool` (optional)
* **Outputs**
  * name `string`
  * transaction `*protocol.Transaction`

### `DeleteTransaction`

Deletes a transaction from map.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * name `string`
* **Outputs**
  * name `string`
  * transaction `*protocol.Transaction`

### `GetTransaction`

Composes a transaction from map to be executed.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * name `string`
* **Outputs**
  * name `string`
  * transaction `*protocol.Transaction`

### `ListTransactions`

Returns a list of transactions currently being composed in the wallet.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
* **Outputs**
  * names `[]string`

### `WriteDataTransaction`

Assigns a data entry to a transaction.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * name `string`
  * writeData `*protocol.WriteData`
  * overwrite `bool` (optional)
* **Outputs**
  * name `string`
  * transaction `*protocol.Transaction`

## Key

Manages keys.

### `FindSigner`

Checks the wallet for a key book and a key matching the given authority set.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * authorities `[]*url.URL`
  * allowBookLoops `bool` (optional)
* **Outputs**
  * paths `[]*api.SigningPath`

### `GenerateAddress`

Generates address based on the provided type.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * type `protocol.SignatureType` (optional)
  * labels `[]string` (optional)
  * force `bool` (optional)
* **Outputs**
  * key `api.Key`

### `ImportKey`

Imports an existing key.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * seed `hex`
  * labels `[]string` (optional)
  * force `bool` (optional)
  * type `protocol.SignatureType`
* **Outputs**
  * address `string`
  * publicKeyHash `hex`
  * key `*api.Key`

### `KeyAssign`

Assigns a new label to a public key.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * oldKeyLabel `string` (optional)
  * newKeyLabel `string` (optional)
  * force `bool` (optional)
* **Outputs**
  * vault `string` (optional)
  * oldKey `api.Key`
  * newKey `api.Key`

### `KeyList`

Returns a list of available keys in the wallet.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
* **Outputs**
  * vault `string` (optional)
  * keyList `[]api.Key`

### `KeyRemove`

Removes a label from a public key.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * oldKeyLabel `string` (optional)
  * newKeyLabel `string` (optional)
  * force `bool` (optional)
* **Outputs**
  * vault `string` (optional)
  * oldKey `api.Key`
  * newKey `api.Key`

### `KeyRename`

Renames a label of a public key.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * oldKeyLabel `string` (optional)
  * newKeyLabel `string` (optional)
  * force `bool` (optional)
* **Outputs**
  * vault `string` (optional)
  * oldKey `api.Key`
  * newKey `api.Key`

### `ResolveKey`

Returns a public key from either a label or keyhash.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * value `string`
  * includePrivateKey `bool` (optional)
* **Outputs**
  * address `string`
  * publicKeyHash `hex`
  * key `*api.Key`

## Ledger

Manages interaction with Ledger hardware wallets.

### `LedgerGenerateKey`

Generates a key on a ledger device.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * labels `[]string` (optional)
  * walletID `string` (optional)
  * sigType `protocol.SignatureType`
* **Outputs**
  * privateKey `hex` (optional)
  * publicKey `hex`
  * keyInfo `api.KeyInfo`
  * lastUsedOn `uint64`
  * labels `*api.KeyLabels` (optional)
  * pages `[]*url.URL`

### `LedgerQueryWallets`

Returns the version of an external ledger device.

* **Inputs**
  * None
* **Outputs**
  * ledgerWalletsInfo `[]*api.LedgerWalletInfo`

## Account

Manages registered accounts.

### `AdiList`

Returns a list of adi's managed by the wallet.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
* **Outputs**
  * adis `[]*api.AdiInfo`

### `ListAccounts`

Lists accounts registered with the wallet.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
* **Outputs**
  * books `[]*api.BookInfo`
  * tokens `[]*api.TokenAccountInfo`

### `RegisterADI`

Registers adi in walletd.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * adi `*url.URL`
  * key `hex` (optional)
* **Outputs**
  * None

### `RegisterBook`

Registers a key book with the wallet.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * url `*url.URL`
  * pages `[]*protocol.KeyPage`
* **Outputs**
  * None

### `RegisterTokenAccount`

Registers a token account with the wallet.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * url `*url.URL`
* **Outputs**
  * None

### `UnregisterBook`

Registers a key book with the wallet.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * url `*url.URL`
* **Outputs**
  * None

### `UnregisterTokenAccount`

Unregisters a token account with the wallet.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * url `*url.URL`
* **Outputs**
  * None

## Signing

Signs transactions, messages, and hashes, and manages recorded timestamps.

### `ResetLastUsedOn`

Resets the timestamp of a key.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * key `string`
* **Outputs**
  * None

### `Sign`

Signs a transaction or an arbitrary hash, using a partial signature as a template.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * publicKey `hex`
  * signer `*url.URL`
  * signerVersion `uint64`
  * timestamp `uint64` (optional)
  * vote `protocol.VoteType` (optional)
  * delegators `[]*url.URL` (optional)
  * hash `hex` (optional)
  * transactionName `string` (optional)
  * transaction `*protocol.Transaction` (optional)
* **Outputs**
  * signature `protocol.Signature`
  * transaction `*protocol.Transaction` (optional)

### `SignMessage`

Signs the hash of arbitrary data passed in message.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * keyName `string`
  * message `hex`
  * isHash `bool` (optional)
  * delegators `[]*url.URL` (optional)
  * signer `*url.URL`
  * signerVersion `uint64`
  * timestamp `uint64` (optional)
  * vote `protocol.VoteType` (optional)
* **Outputs**
  * signature `protocol.Signature`
  * transaction `*protocol.Transaction` (optional)

### `SignTransaction`

Signs a transaction.

* **Inputs**
  * wallet `string`
  * vault `string` (optional)
  * name `string` (optional)
  * transaction `*protocol.Transaction` (optional)
  * keyName `string`
  * signer `*url.URL`
  * signerVersion `uint64`
  * timestamp `uint64` (optional)
  * delegators `[]*url.URL` (optional)
  * initiateOnly `bool` (optional)
* **Outputs**
  * signature `protocol.Signature`
  * transaction `*protocol.Transaction` (optional)
