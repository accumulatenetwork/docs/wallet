# Overview

All interaction with user data (i.e. stored locally, in vaults) is mediated via the daemon. By default the front ends (CLI and GUI) run the daemon internally - that is to say, in-process without launching a server - but the daemon can be launched as server on a Unix socket with the CLI command `accumulate wallet serve-socket`. The daemon can also be launched on a TCP socket but that is **not recommended** as it is considerably less secure.

The daemon's [services.md](services.md "mention") are served over HTTP/JSON-RPC. For example, [#generateaddress](services.md#generateaddress "mention")can be called with:

```json
{
    "jsonrpc": "2.0",
    "id": 1,
    "method": "GenerateAddress",
    "params": {
        "wallet": "/path/to/wallet",
        "labels": [
            "myKey"
        ]
    }
}
```

## Wallets

Most users will only ever use the default wallet, `~/.accumulate/wallet`. Using an alternative wallet location or multiple wallets is not recommended unless the wallet is being used for a testnet. However, the daemon has no notion of a default wallet location, so calls must always specify the wallet location.

## Remote access

The recommended way to provide remote access to a wallet is Unix-socket-over-SSH, as that is much more secure than plain TCP. Use `accumulate wallet serve-socket` to start the daemon on the remote machine and pass `--daemon=ssh-unix://${HOST}//home/${REMOTE_USER}/.accumulate/wallet/daemon.socket` to CLI commands.

## Authentication

Currently, once a vault is unlocked by one client, it is unlocked by all clients. In a future release the daemon will move to a token-based authentication mechanism that requires clients to unlock everything themselves.

When an action requires a vault to be unlocked, the daemon will respond with the following error:

```json
{
    "jsonrpc": "2.0",
    "error": {
        "code": -32803,
        "message": "Unauthenticated",
        "data": {
            "vault": {
                "name": "index",
                "filePath": "/path/to/wallet/index.db"
            },
            "message": "no password specified for encrypted database",
            "exists": true
        }
    },
    "id": 4873
}
```

This error can be intercepted by the front end in order to automatically unlock vaults.
