# CLI Releases

Follow these steps to install the latest released version of the Accumulate Core Wallet CLI.

**Step 1:** Choose one of the following:

{% tabs %}
{% tab title="Windows" %}
{% embed url="https://gitlab.com/accumulatenetwork/core/wallet/-/jobs/artifacts/v0.6.3/file/accumulate-windows-amd64.exe?job=binaries" %}
Intel/AMD
{% endembed %}

{% embed url="https://gitlab.com/accumulatenetwork/core/wallet/-/jobs/artifacts/v0.6.3/file/accumulate-windows-arm64.exe?job=binaries" %}
ARM
{% endembed %}

Rename the file to **accumulate.exe**
{% endtab %}

{% tab title="Mac OS" %}
{% embed url="https://gitlab.com/accumulatenetwork/core/wallet/-/jobs/artifacts/v0.6.3/file/accumulate-darwin-amd64?job=binaries" %}
Intel
{% endembed %}

{% embed url="https://gitlab.com/accumulatenetwork/core/wallet/-/jobs/artifacts/v0.6.3/file/accumulate-darwin-arm64?job=binaries" %}
Apple M1/M2
{% endembed %}

Rename the file to **accumulate**
{% endtab %}

{% tab title="Linux" %}
{% embed url="https://gitlab.com/accumulatenetwork/core/wallet/-/jobs/artifacts/v0.6.3/file/accumulate-linux-amd64?job=binaries" %}
Intel/AMD
{% endembed %}

{% embed url="https://gitlab.com/accumulatenetwork/core/wallet/-/jobs/artifacts/v0.6.3/file/accumulate-linux-arm64?job=binaries" %}
ARM
{% endembed %}

Rename the file to **accumulate**
{% endtab %}
{% endtabs %}

**Step 2:** Move **accumulate** to a folder of your choice.

Moving it to your **documents** folder is recommended, making it easier to locate on your terminal.

**Step 3:** In order to set the permissions so you can run this file, type in the following command: `chmod 744 <path_to_file>`. Pro tip: drag and drop the accumulate file onto the terminal to get the path.

**Step 4:** Copy the binary file location and paste it into your terminal and press enter to bring up the usage guide. Pro-tip: after the first time you drag and drop, you can press the Up arrow key to repeat for every future command.

**Step 5:** Start using the [Accumulate CLI commands.](https://docs.accumulatenetwork.io/accumulate/cli/cli-reference)

**How to access Accumulate CLI from anywhere in the terminal.**

{% hint style="info" %}
**This is an optional step**
{% endhint %}

This step makes it easier to run accumulate since you can call it anywhere in your terminal.

You can follow the instructions in this [link](https://zwbetz.com/how-to-add-a-binary-to-your-path-on-macos-linux-windows/#windows-cli)
