# From source

### Prerequisites

The CLI installation guide needs two requirements.

* Golang 1.20+
* Terminal or any command line interface of your choice

### Setup the repository

```
git clone https://gitlab.com/accumulatenetwork/core/wallet.git
cd wallet
```

### Build

The following instructions work for macOS, Linux, and Windows.

{% tabs %}
{% tab title="Task" %}
Using [Task](https://taskfile.dev) is the easiest way to build the wallet.

1. Install Task: `go install github.com/go-task/task/v3/cmd/task@latest`
2. Add \~/go/bin to your path:
   * Linux and macOS: `export PATH="$PATH:$(go env GOPATH)/bin"`
3. Checkout the version you want to build:
   * `git checkout v0.5.0`
4. Execute the task:
   * GUI: `task build-app`
   * CLI: `task build-cli`

This will create an executable or app bundle in the repository root.
{% endtab %}

{% tab title="GUI (manual)" %}
```bash
git checkout VERSION # e.g. v0.5.0
cd cmd/accumulate-app
go run github.com/wailsapp/wails/v2/cmd/wails build
```

Once you run the command above, a binary (Windows, Linux) or app (macOS) should appear in the build/bin directory.
{% endtab %}

{% tab title="CLI (manual)" %}
```bash
git checkout VERSION # e.g. v0.5.0
go build ./cmd/accumulate
```

Once you run the command above, a binary named `accumulate` will appear in the root directory.
{% endtab %}
{% endtabs %}
