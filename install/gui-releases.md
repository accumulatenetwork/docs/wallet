# GUI Releases

{% tabs %}
{% tab title="Windows" %}
{% embed url="https://gitlab.com/accumulatenetwork/core/wallet/-/jobs/artifacts/v0.6.0/file/accumulate-app-windows-amd64.exe?job=binaries" %}
Intel/AMD
{% endembed %}

{% embed url="https://gitlab.com/accumulatenetwork/core/wallet/-/jobs/artifacts/v0.6.0/file/accumulate-app-windows-arm64.exe?job=binaries" %}
ARM
{% endembed %}

Rename the file to **accumulate-app.exe**
{% endtab %}

{% tab title="Mac OS" %}
{% embed url="https://gitlab.com/accumulatenetwork/core/wallet/-/jobs/artifacts/v0.6.0/browse?job=binaries" %}
Intel
{% endembed %}

{% embed url="https://gitlab.com/accumulatenetwork/core/wallet/-/jobs/artifacts/v0.6.0/browse?job=binaries" %}
Apple M1/M2
{% endembed %}

Rename the file to **accumulate.app**
{% endtab %}

{% tab title="Linux" %}
{% embed url="https://gitlab.com/accumulatenetwork/core/wallet/-/jobs/artifacts/v0.6.0/file/accumulate-app-linux-amd64?job=binaries" %}
Intel/AMD
{% endembed %}

Rename the file to **accumulate-app**
{% endtab %}
{% endtabs %}
