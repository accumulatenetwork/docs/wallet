# Tokens

## Send tokens

### GUI

<figure><img src="../.gitbook/assets/gui-sendTokens.gif" alt=""></figure>

### CLI

`accumulate tx create [sender] [recipient] [amount]`

<figure><img src="../.gitbook/assets/cli-sendTokens.png" alt=""></figure>