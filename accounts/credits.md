# Credits

## Purchasing credits

### GUI

When you select a signer that does not have sufficient credits to execute the
transaction, the GUI will present a button that opens the credit purchase page.

<figure><img src="../.gitbook/assets/gui-buyCredits.gif" alt=""></figure>

### CLI

`accumulate credits [token account] [recipient] [amount] [max ACME (optional)]`

<figure><img src="../.gitbook/assets/cli-buyCredits.png" alt=""></figure>
