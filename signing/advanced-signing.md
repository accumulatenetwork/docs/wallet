# Advanced signing

Each account (e.g. a token account) is governed by one or more authorities (i.e.
key books), which specify a list of signers (i.e. key pages), which may delegate
to other authorities. In most cases, `{ADI}.acme/tokens` is governed by
`{ADI}.acme/book` which has one key page `{ADI}.acme/book/1` which has one or
more keys. In those cases, signing is easy. However if there is any delegation
involved, understanding how to sign a transaction can be difficult. As of
v0.5.0, the wallet will attempt to determine which signing paths are open to you
and present you with a list.

## Determining the authority to sign on behalf of

If the account in question (e.g. `{ADI}.acme/tokens`) has a single authority,
this step is trivial - use that authority. However if the account has multiple
authorities, the wallet will determine which ones you are able to sign on behalf
of and (if there is more than one viable option) ask you which one you wish to
sign with.

This may be overridden with the flag `--sign-for {authority}`. If this flag is
specified, the wallet will attempt to sign on behalf of that authority instead
of automatically determining which authority to sign on behalf of. The value
passed to the flag must be the URL of a key book, otherwise signing will fail.

**Caveat**: the wallet does not have logic for disabled authorities. It treats
all authorities as if they are enabled. Thus, if there are two authorities for
an account, you are able to sign for either, and one is disabled, the wallet
will still ask which one you wish to sign for. If all authorities are disabled
(if the account is 'open'), you must manually specify `--sign-for {authority}`
to sign with an authority other than one of the (disabled) authorities of the
account.

## Determining the key and page to sign with

The wallet attempts to automatically determine which of your keys are present on
a key page that is authorized to sign on behalf of the selected authority. To do
this it scans through all key books and their pages that have been registered
with your wallet to identify pages that you have a key on, and paths (delegated
or direct) from one of those pages to the selected authority. If there is a
single path, the wallet will use that path. Otherwise the wallet will present
you with a list of options to choose from.

You may filter the list of options with the flag `--sign-with {signer}`. This
flag may be specified multiple times. `{signer}` may be a key (name/label or
address), the URL of a key page, or both: `{key}@{page}`. If a key is specified,
the wallet only considers paths using that key. If a key page is specified, the
wallet only considers paths containing that page.

## Registering key books

**The wallet does not scan the network.** It only knows about key books that you
have registered with it. Key books are registered in a number of ways:

  1. When you issue or sign a transaction for an account, the key books that
     govern that account are registered.
  2. When you specify a key book with `--sign-for`, it is registered.
  3. When you specify a key page with `--sign-with`, that page's book is
     registered.
  4. Key books can be manually registered with `accumulate book register {url}`.
  5. Key books can be manually deregistered with `accumulate book unregister {url}`.

In most cases, you should not need to manually register key books - the
automatic processes should be sufficient. But in some complex signing scenarios
you may need to manually register or to use `--sign-with` when you otherwise
would not.