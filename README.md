# Overview

The Accumulate Core Wallet is a desktop wallet with a command line interface (CLI) and a graphical interface (GUI/app). The Accumulate Core Wallet is the reference implementation for all Accumulate wallets.



<table data-view="cards"><thead><tr><th></th><th data-hidden data-card-target data-type="content-ref"></th></tr></thead><tbody><tr><td><strong>Install</strong></td><td><a href="install/gui-releases.md">gui-releases.md</a></td></tr><tr><td><strong>Setup your wallet</strong></td><td><a href="vaults/setup-your-wallet.md#initializing-the-wallet">#initializing-the-wallet</a></td></tr><tr><td><strong>Migrate from v0.4</strong></td><td><a href="vaults/managing-vaults.md#migrating-from-v0.4-and-earlier">#migrating-from-v0.4-and-earlier</a></td></tr></tbody></table>
